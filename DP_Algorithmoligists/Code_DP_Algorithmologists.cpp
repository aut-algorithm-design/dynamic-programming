#include<bits/stdc++.h>
#define MAXN 10000
#define MAXK 100

using namespace std;

int dp[MAXN][MAXK];
int operating_cost[MAXN][MAXK];
int relocation_cost[MAXK][MAXK];
int parent[MAXN][MAXK];
string city_name[MAXK];

int main(){
    std::fstream myfile("input_dp.txt", std::ios_base::in);

    int n,k;
    myfile>>n>>k;

    for(int j=1 ; j<=k ; j++){
        myfile>>city_name[j];
    }

    for(int j=1 ; j<= k ; j++){
        for(int i = 1 ; i<= n ; i++ ){
            myfile>>operating_cost[i][j];
        }
    }

    for(int j=1 ; j<= k ; j++){
        for(int i = 1 ; i<= k ; i++ ){
            myfile>>relocation_cost[i][j];
        }
    }

    for(int j=1 ; j<=k ; j++){
        dp[1][j] = operating_cost[1][j];
    }


    for(int i=2 ; i<= n ; i++){
        for(int j=1 ; j<=k ; j++){
            dp[i][j] = INT_MAX;
            for(int y=1 ; y<= k ; y++){
                int new_dp = dp[i-1][y]+relocation_cost[y][j]+operating_cost[i][j];
                if(new_dp<dp[i][j]){
                    dp[i][j] = new_dp;
                    parent[i][j] = y;

                }
            }
        }
    }

    int answer = INT_MAX;
    vector<int> city_answer;



/*
    for(int j=1 ; j<=k ; j++){
        for(int i=1 ; i<= n ; i++){
            cout<<dp[i][j]<<" ";
        }
        cout<<endl;
    }
    cout<<"######################################\n";
*/
    int last_city;
    for(int j=1 ; j<= k ; j++){
        if(dp[n][j]<answer){
            answer =dp[n][j];
            last_city = j;
        }
    }
    for(int i=n ; i>=1 ; i--){
        city_answer.push_back(last_city);
        last_city = parent[i][last_city];
    }

    cout<<answer<<endl;
    while(city_answer.size()){
        cout<<city_name[city_answer[city_answer.size()-1]]<<" ";
        city_answer.pop_back();
    }


    return 0;
}
